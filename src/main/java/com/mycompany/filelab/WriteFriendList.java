/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.stream.FileCacheImageOutputStream;

/**
 *
 * @author ASUS
 */
public class WriteFriendList {
    public static void main(String[] args) {
        LinkedList<Friend> friendList = new LinkedList<>();
        friendList.add(new Friend("Worawit", 43, "0881234567"));
        friendList.add(new Friend("Jakaman", 45, "0887654321"));
        friendList.add(new Friend("Moowhan", 45, "0884321765"));
        FileOutputStream fos = null;
        try {
            File file = new File("list_of_friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friendList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
